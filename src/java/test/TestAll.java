package test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import test.core.TestProperties;
import test.core.TestRunner;
import test.core.UnitTest;
import test.core.Util;

/**
 * A parameterized Junit test to test JastAddJ-Intraflow
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
@RunWith(Parameterized.class)
public class TestAll {

	private static final TestProperties properties = new TestProperties();
	static {
		//properties.put("nodeNbrs", "true");
	}

	private final UnitTest unitTest;

	/**
	 * Construct a new JastAdd test
	 * @param unitTest the test to run.
	 */
	public TestAll(UnitTest unitTest) {
		this.unitTest = unitTest;
	}

	/**
	 * Run the JastAdd test
	 */
	@Test
	public void runTest() {
		TestRunner.runTest(unitTest, properties);
	}

	@SuppressWarnings("javadoc")
	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return Util.getTests(properties);
	}
}
