package test.core;

import java.io.File;
import java.util.Properties;

import static org.junit.Assert.fail;

/**
 * Configuration for a single test - includes info about the test to run, the
 * test root directory, and JastAdd configuration for the test run.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class TestConfiguration {
	protected final JastAddConfiguration jastadd;
	protected File testDir;
	protected final UnitTest unitTest;
	protected final TestProperties testProperties;
	protected final Result expected;
	protected final File tmpDir;

	/**
	 * @param testRoot
	 * @param test
	 * @param jastaddProperties
	 */
	public TestConfiguration(String testRoot, UnitTest test, Properties jastaddProperties) {
		unitTest = test;
		testDir = new File(testRoot, unitTest.getPath());
		jastadd = JastAddConfiguration.get(jastaddProperties);
		testProperties = Util.getProperties(new File(testDir, "Test.properties"));
		expected = getExpectedResult(testProperties);
		jastadd.addOptions(unitTest.getOptions());

		tmpDir = setupTemporaryDirectory(testDir, unitTest);
	}

	private static Result getExpectedResult(Properties props) {
		if (!props.containsKey("result"))
			return Result.EXEC_PASSED;

		String result = props.getProperty("result");
		if (result.equals("JASTADD_PASSED") || result.equals("JASTADD_PASS")) {
			return Result.JASTADD_PASSED;
		} else if (result.equals("JASTADD_FAILED") || result.equals("JASTADD_FAIL")) {
			return Result.JASTADD_FAILED;
		} else if (result.equals("JASTADD_ERR_OUTPUT") || result.equals("JASTADD_ERR")) {
			return Result.JASTADD_ERR_OUTPUT;
		} else if (result.equals("COMPILE_PASSED") || result.equals("COMPILE_PASS")) {
			return Result.COMPILE_PASSED;
		} else if (result.equals("COMPILE_FAILED") || result.equals("COMPILE_FAIL")) {
			return Result.COMPILE_FAILED;
		} else if (result.equals("EXEC_PASSED") || result.equals("EXEC_PASS")) {
			return Result.EXEC_PASSED;
		} else if (result.equals("EXEC_FAILED") || result.equals("EXEC_FAIL")) {
			return Result.EXEC_FAILED;
		} else if (result.equals("OUTPUT_PASSED") || result.equals("OUTPUT_PASS")) {
			return Result.OUTPUT_PASSED;
		} else {
			fail("Unknown result option: " + result);
			return Result.OUTPUT_PASSED;
		}
	}

	/**
	 * Set up the temporary directory - create it if it does not exist
	 * and clean it if it does already exist.
	 * @param testDir The test directory
	 * @param unitTest
	 */
	private static File setupTemporaryDirectory(File testDir, UnitTest unitTest) {
		String tmpDirName;
		if (unitTest.hasSeveralFlagSets()) {
			tmpDirName = testDir.getPath() + "-" + unitTest.getFlagSetIndex();
		} else {
			tmpDirName = testDir.getPath();
		}

		if (tmpDirName.startsWith("tests")) {
			tmpDirName = tmpDirName.substring(6);
		}

		File tmpDir = new File("tmp" + File.separator + tmpDirName);

		if (!tmpDir.exists()) {
			// create directory with intermediate parent directories
			tmpDir.mkdirs();
		} else {
			// clean temporary directory
			cleanDirectory(tmpDir);
		}
		return tmpDir;
	}

	/**
	 * Recursively remove all files and folders in a directory
	 * @param dir The directory to nuke
	 */
	private static void cleanDirectory(File dir) {
		for (File file: dir.listFiles()) {
			if (!file.isDirectory()) {
				file.delete();
			} else {
				cleanDirectory(file);
				file.delete();
			}
		}
	}
}
