package test.core;

import java.util.LinkedList;
import java.util.List;

/**
 * JastAdd3 command-line option
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JastAdd3Option extends JastAddOption {

	private final String OPTION_PREFIX = "-";
	private final String VALUE_SEPARATOR = " ";

	/**
	 * Basic command-line option
	 * @param name
	 */
	public JastAdd3Option(String name) {
		super(name);
	}

	/**
	 * Key-value command-line option
	 * @param name
	 * @param value
	 */
	public JastAdd3Option(String name, String value) {
		super(name, value);
	}

	@Override
	public String toString() {
		return OPTION_PREFIX + name + (hasValue ? VALUE_SEPARATOR + value : "");
	}

	/**
	 * @return The option as a list of strings
	 */
	@Override
	public List<String> toList() {
		List<String> list = new LinkedList<String>();
		list.add(OPTION_PREFIX + name);
		if (hasValue) {
			list.add(value);
		}
		return list;
	}
}
