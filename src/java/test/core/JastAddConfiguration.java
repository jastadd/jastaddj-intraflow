package test.core;

import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

/**
 * A configuration to run a JastAdd instance.
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JastAddConfiguration {

	private final String path;
	private final Set<JastAddOption> options = new HashSet<JastAddOption>();
	private ClassLoader classLoader = null;

	/**
	 * Create a new JastAdd configuration to run the JastAdd version
	 * in the given jar file.
	 * @param jarPath
	 */
	public JastAddConfiguration(String jarPath) {
		this.path = jarPath;
	}

	/**
	 * Add a command-line option
	 * @param name
	 */
	public void addOption(String name) {
		options.add(buildOption(name));
	}

	/**
	 * Add a command-line option with a value
	 * @param name
	 * @param value
	 */
	public void addOption(String name, String value) {
		options.add(buildOption(name, value));
	}

	/**
	 * Remove a command-line option
	 * @param name
	 */
	public void removeOption(String name) {
		options.remove(new JastAddOption(name));
	}

	/**
	 * Invoke this JastAdd configuration on the given directory and fail
	 * JUnit test when result does not match the expected result.
	 * @param config Test configuration
	 */
	public void invoke(TestConfiguration config) {
		File tmpDir = config.tmpDir;
		String testDirectory = config.testDir.getPath();
		Result expected = config.expected;

		if (hasOption("jjtree")) {
			invokeJJTree(tmpDir, testDirectory);
			invokeJavaCC(tmpDir, testDirectory);
		}

		StringBuilder errors = new StringBuilder();
		int exitValue = invokeJastAdd(config, errors);

		try {
			// write errors to jastadd.err in tmpDir
			PrintWriter errFile = new PrintWriter(new File(tmpDir, "jastadd.err"));
			errFile.append(errors.toString());
			errFile.close();
		} catch (IOException e) {
			System.err.println("Failed to write JastAdd error output file");
		}

		if (exitValue == 0) {
			// code generation passed
			if (expected == Result.JASTADD_FAILED) {
				fail("JastAdd code generation passed when expected to fail");
			}
		} else {
			// code generation failed
			if (expected != Result.JASTADD_FAILED && expected != Result.JASTADD_ERR_OUTPUT) {
				fail("JastAdd code generation failed when expected to pass:\n" +
					errors.toString());
			}
		}
	}

	private int invokeJastAdd(TestConfiguration config, StringBuilder errors) {
		if (classLoader != null) {
			// not forked mode
			ByteArrayOutputStream stdOut = new ByteArrayOutputStream();
			ByteArrayOutputStream errOut = new ByteArrayOutputStream();
			try {
				Class<?> jastAdd = Class.forName("org.jastadd.JastAdd", true, classLoader);
				Method main = jastAdd.getMethod("compile", String[].class,
						PrintStream.class, PrintStream.class);
				int exitValue = (Integer) main.invoke(null,
						buildOptionArray(config),
						new PrintStream(stdOut),
						new PrintStream(errOut));
				return exitValue;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} finally {
				errors.append(errOut.toString());
			}

		} else {
			// forked mode
			String command = "java -jar " + path;
			String optionString = buildOptionString(config);
			String commandLine = command + " " + optionString;

			try {
				Process p = Runtime.getRuntime().exec(commandLine);
				Scanner err = new Scanner(p.getErrorStream());
				while (err.hasNextLine()) {
					errors.append(err.nextLine());
					errors.append("\n");
				}
				err.close();
				int exitValue = p.waitFor();
				return exitValue;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		return 1;
	}

	private boolean hasOption(String string) {
		for (JastAddOption option: options) {
			if (option.equals(string))
				return true;
		}
		return false;
	}

	private void invokeJJTree(File tmpDir, String testDir) {
		String command = "java -classpath lib/javacc.jar org.javacc.jjtree.Main " +
				"-OUTPUT_DIRECTORY=" + tmpDir + " " + testDir + "/Test.jjt";
		StringBuffer errors = new StringBuffer();
		try {
			Process p = Runtime.getRuntime().exec(command);
			Scanner err = new Scanner(p.getErrorStream());
			while (err.hasNextLine()) {
				errors.append(err.nextLine());
				errors.append("\n");
			}
			err.close();
			int exitValue = p.waitFor();
			if (exitValue != 0) {
				fail("JJTree code generation failed!\n" + errors);
			}
		} catch (IOException e) {
			fail("JJTree code generation failed!\n" + e.getMessage());
		} catch (InterruptedException e) {
			fail("JJTree code generation failed!\n" + e.getMessage());
		}
	}

	private void invokeJavaCC(File tmpDir, String testDir) {
		String command = "java -classpath lib/javacc.jar org.javacc.parser.Main " +
				"-OUTPUT_DIRECTORY=" + tmpDir + " " + tmpDir + "/Test.jj";
		StringBuffer errors = new StringBuffer();
		try {
			Process p = Runtime.getRuntime().exec(command);
			Scanner err = new Scanner(p.getErrorStream());
			while (err.hasNextLine()) {
				errors.append(err.nextLine());
				errors.append("\n");
			}
			err.close();
			int exitValue = p.waitFor();
			if (exitValue != 0) {
				fail("JavaCC code generation failed!\n" + errors);
			}
		} catch (IOException e) {
			fail("JavaCC code generation failed!\n" + e.getMessage());
		} catch (InterruptedException e) {
			fail("JavaCC code generation failed!\n" + e.getMessage());
		}
	}

	private String[] buildOptionArray(TestConfiguration config) {
		List<String> args = new ArrayList<String>();

		// set output directory
		args.addAll(buildOutputDirOption(config.tmpDir.getPath()).toList());

		addExtraOptions(args);

		for (JastAddOption option: options) {
			args.addAll(option.toList());
		}

		String sources = config.testProperties.getProperty("sources", "");
		if (sources.isEmpty()) {
			// find JastAdd source files in test directory:
			Iterable<String> sourceFiles = findSources(config.testDir);
			for (String path: sourceFiles) {
				args.add(path);
			}
		} else {
			// use source list from test properties
			for (String sourceFile: sources.split(",")) {
				sourceFile = sourceFile.replace('/', File.separatorChar);
				args.add(config.testDir.getPath() + File.separator + sourceFile);
			}
		}

		String[] array = new String[args.size()];
		for (int i = 0; i < args.size(); ++i) {
			array[i] = args.get(i);
		}
		return array;
	}

	private String buildOptionString(TestConfiguration config) {
		String[] args = buildOptionArray(config);
		StringBuilder buf = new StringBuilder();

		for (int i = 0; i < args.length; ++i) {
			if (i > 0) {
				buf.append(" ");
			}
			buf.append(args[i]);
		}

		return buf.toString();
	}

	protected void addExtraOptions(List<String> options) {
		// place to add extra options if needed
	}

	protected JastAddOption buildOutputDirOption(String path) {
		return new JastAddOption("o", path);
	}

	protected JastAddOption buildOption(String name) {
		return new JastAddOption(name);
	}

	protected JastAddOption buildOption(String name, String value) {
		return new JastAddOption(name, value);
	}

	private Iterable<String> findSources(File testDir) {
		List<String> sources = new ArrayList<String>();
		for (File file: testDir.listFiles()) {
			if (!file.isDirectory()) {
				String fileName = file.getName();
				if (fileName.endsWith(".jrag") || fileName.endsWith(".jadd") ||
						fileName.endsWith(".ast") || fileName.endsWith(".config")) {
					sources.add(file.getPath());
				}
			}
		}
		Collections.sort(sources);
		return sources;
	}

	/**
	 * Add the options in the option string
	 * @param optionString
	 */
	public void addOptions(String optionString) {
		if (optionString.trim().isEmpty()) {
			return;
		}
		String[] opts = optionString.trim().split("\\s+");
		for (int i = 0; i < opts.length; ++i) {
			String option = opts[i];
			if (option.startsWith("--")) {
				option = option.substring(2);
			}
			if (option.contains("=")) {
				String[] parts = option.split("=");
				addOption(parts[0], parts[1]);
			} else {
				addOption(option);
			}
		}
	}

	/**
	 * @param jastaddProperties
	 * @return A JastAdd configuration with the options given in the properties
	 */
	public static final JastAddConfiguration get(Properties jastaddProperties) {
		boolean fork = jastaddProperties.getProperty("fork", "true").equalsIgnoreCase("true");
		String jarPath = jastaddProperties.getProperty("jastadd.jar", "");
		boolean jastadd3 = jastaddProperties.getProperty("jastadd3", "false").equalsIgnoreCase("true");
		String options = jastaddProperties.getProperty("options", "");

		JastAddConfiguration config;

		if (jastadd3) {
			config = new JastAdd3Configuration(jarPath);
		} else {
			config = new JastAddConfiguration(jarPath);
		}

		if (!fork) {
			// set ClassLoader
			try {
				File jarFile = new File(jarPath);
				URL jarUrl = jarFile.toURI().toURL();
				config.classLoader = new URLClassLoader(new URL[] { jarUrl });
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}

		if (!options.isEmpty()) {
			config.addOptions(jastaddProperties.getProperty("options"));
		}

		return config;
	}
}
