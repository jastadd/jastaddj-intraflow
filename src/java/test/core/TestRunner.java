package test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility methods for running JastAddJ-Intraflow unit tests.
 * 
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * @author Harald Görtz <harald.gortz@gmail.com>
 */
public class TestRunner {

	private static String SYS_LINE_SEP = System.getProperty("line.separator");

	/**
	 * Run the unit test in testDir with the given test suite properties.
	 * 
	 * @param testName
	 * @param testSuiteProperties
	 */
	public static void runTest(UnitTest unitTest, Properties testSuiteProperties) {
		TestConfiguration config = new TestConfiguration(Util.TEST_ROOT,
				unitTest, testSuiteProperties);
		try {
			writeGraphSpec(config.tmpDir, config.testDir,
					config.testProperties, testSuiteProperties);
		} catch (Exception e1) {
			fail(stackTraceAsString(e1));
		}
		// Compare the output with the expected output
		compareOutput(config.tmpDir, config.testDir);
	}

	/**
	 * Compare the generated output to the expected output
	 * 
	 * @param tmpDir
	 *            where to put output files
	 * @param testDir
	 *            location of input files
	 * @param dotCommand
	 *            command that runs Graphviz
	 */
	private static void compareOutput(File tmpDir, File testDir) {
		try {
			File expectedFile = new File(testDir, "out.expected");
			File actualFile = new File(tmpDir, "out.dot");
			String expected = readFileToString(expectedFile);
			String actual = readFileToString(actualFile);
			String diffGraph = generateGraphDiff(tmpDir, expected, actual);
			if (!diffGraph.isEmpty()) {
				assertEquals("Output files differ", expected, actual);
			}

		} catch (IOException e) {
			fail("IOException occurred while comparing output: "
					+ e.getMessage());
		}
	}

	/**
	 * <p>
	 * Reads an entire file to a string object.
	 * 
	 * <p>
	 * If the file does not exist an empty string is returned.
	 * 
	 * <p>
	 * The system dependent line separator char sequence is replaced by the
	 * newline character.
	 * 
	 * @param file
	 * @return the contents of the file
	 * @throws FileNotFoundException
	 */
	private static String readFileToString(File file)
			throws FileNotFoundException {
		if (!file.isFile())
			return "";

		Scanner scanner = new Scanner(file);
		scanner.useDelimiter("\\Z");
		String theString = scanner.hasNext() ? scanner.next() : "";
		theString = theString.replace(SYS_LINE_SEP, "\n").trim();
		scanner.close();
		return theString;
	}

	/**
	 * Write a dot graph specification of the test program to a file
	 * 
	 * @param tmpDir
	 *            where to put output files
	 * @param testDir
	 *            path to input files
	 * @param testProperties
	 *            properties of the current unit test
	 * @param testSuiteProperties
	 *            properties of the test suite
	 */
	private static void writeGraphSpec(File tmpDir, File testDir,
			TestProperties testProperties, Properties testSuiteProperties)
			throws Exception {
		File inFile = new File(testDir, "Test.input");
		List<String> args = new LinkedList<String>();
		if (testProperties.getProperty("options") != null) {
			args.addAll(java.util.Arrays.asList(testProperties.getProperty(
					"options").split(",")));
		}
		if (testSuiteProperties.getProperty("nodeNbrs") != null
				&& !args.contains("--nodeNbrs")) {
			args.add("--nodeNbrs");
		}
		args.add(inFile.getAbsolutePath());

		TestFrontend frontend = new TestFrontend();
		int exitCode = frontend.run(args.toArray(new String[args.size()]));
		if (exitCode != 0) {
			fail("JastAddJ returned exit code " + exitCode);
		}

		Writer out = new FileWriter(new File(tmpDir, "out.dot"));
		out.write(frontend.getCFGDotSpec());
		out.close();
	}

	/**
	 * Write a dot graph specification of the test program with differences to
	 * the expected output marked in red. The graphs are considered equal if
	 * they have the same nodes and edges.
	 * 
	 * @param tmpDir
	 *            where to put output files
	 * @param expected
	 * @param actual
	 * @param testDir
	 *            path to input files
	 * @return true
	 */
	private static String generateGraphDiff(File tmpDir, String expected,
			String actual) {
		boolean same = true;

		StringTokenizer stEx = new StringTokenizer(expected, SYS_LINE_SEP);
		StringTokenizer stAc = new StringTokenizer(actual, SYS_LINE_SEP);

		List<String> exLines = new LinkedList<String>();
		List<String> exLinesSimple = new LinkedList<String>();
		while (stEx.hasMoreTokens()) {
			String line = stEx.nextToken();
			if (!ignore(line) && !isStaticLine(line)) {
				exLines.add(line);
				exLinesSimple.add(simplifyLine(line));
			}
		}

		AST.BodyDecl bd = new AST.InstanceInitializer();
		StringBuffer sb = new StringBuffer(bd.dotSpecIntro());

		while (stAc.hasMoreTokens() && !exLines.isEmpty()) {
			String ac = stAc.nextToken();
			if (ignore(ac)) {
				if (!isStaticLine(ac)) {
					sb.append(ac).append(SYS_LINE_SEP);
				}
				continue;
			}
			String acSimple = simplifyLine(ac);
			if (exLinesSimple.contains(acSimple)) {
				sb.append(ac).append(SYS_LINE_SEP);
				int index = exLinesSimple.indexOf(acSimple);
				exLinesSimple.remove(index);
				exLinesSimple.add(index, null);
			} else {
				same = false;
				sb.append(formatUnexpectedLine(ac)).append(SYS_LINE_SEP);
			}
		}

		while (stAc.hasMoreTokens()) {
			String ac = stAc.nextToken();
			if (ignore(ac)) {
				if (!isStaticLine(ac)) {
					sb.append(ac).append(SYS_LINE_SEP);
				}
				continue;
			}
			same = false;
			sb.append(formatUnexpectedLine(ac)).append(SYS_LINE_SEP);
		}

		for (int i = 0; i < exLinesSimple.size(); i++) {
			String ex = exLinesSimple.get(i);
			if (ex != null) {
				same = false;
				sb.append(formatMissingLine(exLines.get(i))).append(SYS_LINE_SEP);
			}
		}
		sb.append(bd.dotSpecOutro());
		if (!same) {
			try {
				FileWriter out = new FileWriter(new File(tmpDir,
						"diffGraph.dot"));
				out.write(sb.toString());
				out.close();
			} catch (IOException e) {
				fail(stackTraceAsString(e));
			}
		}
		return same ? "" : sb.toString();
	}

	/**
	 * Test if a line should generate a visible artifact if it differs from the
	 * expected output.
	 * 
	 * @param s
	 *            the line to investigate
	 * @return true if the line is equal to some uninteresting line, and false
	 *         otherwise
	 */
	private static boolean ignore(String s) {
		String s2 = s.trim();
		// return s2.equals("digraph {") || s2.startsWith("node [")
		// || s2.equals("}") || s2.startsWith("//") || s2.isEmpty();
		return !s2.contains("->") || s2.contains("style=invis")
				|| s2.startsWith("//") || s2.contains("dir=none");
	}

	/**
	 * Test if a line is one of those that are the same in every graph
	 * specification.
	 * 
	 * @param s
	 *            the line to investigate
	 * @return true if the line is static, false otherwise
	 */
	private static boolean isStaticLine(String s) {
		String s2 = s.trim();
		return s2.equals("digraph {") || s2.startsWith("node [")
				|| s2.equals("}");
	}

	/**
	 * Remove uninteresting elements from an edge specification. Uninteresting
	 * means, in this context, anything except the two node names and the arrow.
	 * 
	 * @param s
	 *            the line to simplify
	 * @return the simplified line
	 */
	private static String simplifyLine(String s) {
		StringBuilder sb = new StringBuilder(s);
		int index = sb.indexOf("//");
		if (index > 0) {
			sb.delete(index, sb.length());
		}
		index = sb.indexOf(":succ");
		if (index > 0) {
			sb.delete(index, index + 5);
		}
		index = sb.indexOf(":name");
		if (index > 0) {
			sb.delete(index, index + 5);
		}
		index = sb.indexOf("[");
		if (index > 0) {
			sb.delete(index, sb.length());
		}
		index = sb.indexOf(";");
		if (index > 0) {
			sb.deleteCharAt(index);
		}
		Pattern ws = Pattern.compile("\\s");
		Matcher m = ws.matcher(sb);
		return m.replaceAll("");
	}

	/**
	 * Modifies a line so that its corresponding component will show up red in
	 * the comparison image.
	 * 
	 * @param s
	 *            the line to modify
	 * @return the modified line
	 */
	private static String formatUnexpectedLine(String s) {
		StringBuffer sb = new StringBuffer("/* MISMATCHED LINE */");
		String[] split = s.split("\\[", 2);
		if (split.length > 1) {
			sb.append(split[0]);
			sb.append("[color=\"red\",");
			sb.append(split[1]);
		} else {
			sb.append(s, 0, s.length() - 1); // -1 for trailing semicolon
			sb.append(" [color=\"red\"];");
		}
		return sb.toString();
	}

	/**
	 * Modifies a line so that its corresponding component will show up red with
	 * dashed lines in the comparison image.
	 * 
	 * @param s
	 *            the line to modify
	 * @return the modified line
	 */
	private static String formatMissingLine(String s) {
		Pattern ws = Pattern.compile("\\s");
		Matcher m = ws.matcher(s);
		StringBuffer sb = new StringBuffer(m.replaceAll(""));
		
		//it doesn't matter if the entry and exit nodes get
		//these port names, even if they are not defined
		if (!s.contains(":succ")) {
			sb.insert(sb.indexOf("->"), ":succ");
		}
		if (!s.contains(":name")) {
			int index = sb.indexOf("[");
			if (index < 0) {
				index = sb.indexOf(";");
			}
			if (index > 0) {
				sb.insert(index, ":name");
			} else {
				sb.append(":name");
			}
		}
		if (!s.contains(";")) {
			sb.append(";");
		}
		sb = new StringBuffer(formatUnexpectedLine(sb.toString()));
		int i = sb.indexOf("[");
		if (i > 0) {
			sb.insert(sb.indexOf("[") + 1, "style=\"dashed\",");
		} else {
			sb.insert(sb.indexOf(";"), "[color=\"red\",style=\"dashed\"]");
		}
		return sb.toString();
	}

	private static String stackTraceAsString(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return SYS_LINE_SEP + sw.toString();
	}

}
