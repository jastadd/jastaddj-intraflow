package test.core;

import java.util.Collections;
import java.util.List;

/**
 * A command-line option
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JastAddOption {
	protected String name;
	protected String value;
	protected boolean hasValue = false;

	private final String OPTION_PREFIX = "--";
	private final String VALUE_SEPARATOR = "=";

	/**
	 * Creates a command-line option without a value
	 * @param name
	 */
	public JastAddOption(String name) {
		this.name = name;
	}

	/**
	 * Creates a command-line option with a value
	 * @param name
	 * @param value
	 */
	public JastAddOption(String name, String value) {
		this.name = name;
		this.value = value;
		this.hasValue = true;
	}

	@Override
	public String toString() {
		return OPTION_PREFIX + name + (hasValue ? VALUE_SEPARATOR + value : "");
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o instanceof JastAddOption)  {
			JastAddOption other = (JastAddOption) o;
			return other.name.equals(name);
		} else {
			String other = "" + o;
			return other.equalsIgnoreCase(name);
		}
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	/**
	 * @return The option as a list of strings
	 */
	public List<String> toList() {
		return Collections.singletonList(OPTION_PREFIX + name +
				(hasValue ? VALUE_SEPARATOR + value : ""));
	}
}
