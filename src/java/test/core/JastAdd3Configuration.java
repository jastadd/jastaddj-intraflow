package test.core;

import java.util.List;

/**
 * JastAdd3 configuration
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class JastAdd3Configuration extends JastAddConfiguration {

	/**
	 * Construct a JastAdd3 configuration
	 * @param jarPath
	 */
	public JastAdd3Configuration(String jarPath) {
		super(jarPath);
	}

	@Override
	protected void addExtraOptions(List<String> options) {
		super.addExtraOptions(options);

		options.addAll(buildOption("source_output").toList());
	}

	@Override
	protected JastAddOption buildOutputDirOption(String path) {
		return new JastAdd3Option("d", path);
	}

	@Override
	protected JastAddOption buildOption(String name) {
		return new JastAdd3Option(name);
	}

	@Override
	protected JastAddOption buildOption(String name, String value) {
		return new JastAdd3Option(name, value);
	}

}
