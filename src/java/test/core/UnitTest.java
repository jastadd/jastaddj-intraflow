package test.core;

/**
 * Describes one test with a particular set of options.
 */
public class UnitTest {
	/**
	 * The path from the test root to the unit test.
	 **/
	private final String path;

	/**
	 * JastAdd options from properties file
	 */
	private final String options;

	/**
	 * If the test case has more than one flag set
	 */
	private final boolean severalFlagSets;

	/**
	 * Used to create different temp directories for each flag set
	 */
	private final int flagSetIndex;

	/**
	 * @param path
	 * @param options
	 * @param hasSeveralFlags
	 * @param flagSetIndex
	 */
	public UnitTest(String path, String options, boolean hasSeveralFlags, int flagSetIndex) {
		this.path = path;
		this.options = options;
		this.severalFlagSets = hasSeveralFlags;
		this.flagSetIndex = flagSetIndex;
	}

	@Override
	public String toString() {
		if (hasSeveralFlagSets()) {
			return path + " - " + flagSetIndex + " - " + options;
		} else {
			return path;
		}
	}

	/**
	 * @return The test path.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return The test options.
	 */
	public String getOptions() {
		return options;
	}

	/**
	 * @return index in flag set
	 */
	public int getFlagSetIndex() {
		return flagSetIndex;
	}

	/**
	 * @return {@code true} if there are several flag sets.
	 */
	public boolean hasSeveralFlagSets() {
		return severalFlagSets;
	}
}
