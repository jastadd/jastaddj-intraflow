package test.core;

import java.util.LinkedList;

import AST.BytecodeParser;
import AST.CompilationUnit;
import AST.Frontend;
import AST.JavaParser;

/**
 * Frontend for testing JastAddJ-Intraflow
 * 
 * @author dt09hg0
 * 
 */
public class TestFrontend extends Frontend {

	private final JavaParser parser;
	private final BytecodeParser bytecodeParser;

	private LinkedList<AST.CompilationUnit> compilationUnits;

	public TestFrontend() {
		parser = new JavaParser() {
			@Override
			public CompilationUnit parse(java.io.InputStream is, String fileName)
					throws java.io.IOException, beaver.Parser.Exception {

				return new parser.JavaParser().parse(is, fileName);
			}
		};
		bytecodeParser = new BytecodeParser();
		compilationUnits = new LinkedList<CompilationUnit>();
	}

	public static void main(String args[]) {
		int exitCode = new TestFrontend().run(args);
		if (exitCode != 0) {
			System.exit(exitCode);
		}
	}

	public int run(String args[]) {
		return run(args, bytecodeParser, parser);
	}

	protected void processNoErrors(CompilationUnit unit) {
		compilationUnits.add(unit);
	}

	/**
	 * Get the dot specification of the CFG for the first compilation unit
	 * 
	 * @return the CFG dot spec as a String
	 */
	public String getCFGDotSpec() {
		if (compilationUnits.size() > 0) {
			return compilationUnits.getFirst().getTypeDecl(0).getBodyDecl(0)
					.CFGDotSpec();
		} else {
			return "";
		}
	}

	@Override
	protected void initOptions() {
		super.initOptions();
		AST.Options options = program.options();
		options.addKeyOption("--rigid");
		options.addKeyOption("--nodeNbrs");
	}

	@Override
	protected String name() {
		return "JastAddJ-IntraFlow";
	}

	@Override
	protected String version() {
		return "To be added";
	}
}
