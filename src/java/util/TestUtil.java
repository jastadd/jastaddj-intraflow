package util;

import java.util.ArrayList;
import java.util.Iterator;

import AST.*;

public class TestUtil {

	/**
	 * Prints the control flow of a body declaration. 
	 * @param root The root of the body declaration.
	 * @return The flow as a string.
	 */
	public static String printCFG(BodyDecl root) {
		StringBuffer res = new StringBuffer();
		SmallSet<CFGNode> visitedList = SmallSet.mutable();
		ArrayList<CFGNode> worklist = new ArrayList<CFGNode>();
		CFGEntry entry = root.entry();
		worklist.add(entry);
		visitedList.add(entry);
		while (!worklist.isEmpty()) {
			CFGNode node = worklist.remove(0);
			res.append(succToString(node));
			Iterator<CFGNode> itr = node.succ().iterator();
			while (itr.hasNext()) {
				CFGNode succ = itr.next();
				if (!visitedList.contains(succ)) {
					worklist.add(succ);
					visitedList.add(succ);
				}
			}
		}
		return res.toString();
	}
	
	/**
	 * Prints the reverse control flow of a body declaration. 
	 * @param root The root of the body declaration.
	 * @return The flow as a string.
	 */
	public static String printReverseCFG(BodyDecl root) {
		StringBuffer res = new StringBuffer();
		SmallSet<CFGNode> visitedList = SmallSet.mutable();
		ArrayList<CFGNode> worklist = new ArrayList<CFGNode>();
		CFGExit exit = root.exit();
		worklist.add(exit);
		visitedList.add(exit);
		while (!worklist.isEmpty()) {
			CFGNode node = worklist.remove(0);
			res.append(predToString(node));
			Iterator<CFGNode> itr = node.succ().iterator();
			while (itr.hasNext()) {
				CFGNode pred = itr.next();
				if (!visitedList.contains(pred)) {
					worklist.add(pred);
					visitedList.add(pred);
				}
			}
		}
		return res.toString();
	}

	
	private static String succToString(CFGNode node) {
		StringBuffer res = new StringBuffer();
		for (CFGNode succ : node.succ()) {
			res.append(node + "->" + succ).append('\n');
		}
		return res.toString();
	}

	private static String predToString(CFGNode node) {
		StringBuffer res = new StringBuffer();
		for (CFGNode pred : node.pred()) {
			res.append(pred + "->" + node).append('\n');
		}
		return res.toString();
	}
}
