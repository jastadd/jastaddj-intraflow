package util;

import AST.BytecodeParser;
import AST.CompilationUnit;
import AST.Frontend;
import AST.JavaParser;

/**
 * Prints a dot specification of the AST of the first body declaration to System.out
 * @author dt09hg0
 *
 */
public class ASTDotGraphPrinter extends Frontend {
	public static void main(String args[]) {
		printASTGraph(args);
	}

	public static int printASTGraph(String args[]) {
		int result = new ASTDotGraphPrinter().run(
				args,
				new BytecodeParser(),
				new JavaParser() {
					@Override
					public CompilationUnit parse(java.io.InputStream is,
							String fileName) throws java.io.IOException,
							beaver.Parser.Exception {

						return new parser.JavaParser().parse(is, fileName);
					}
				});
		return result;
	}

	protected void processNoErrors(CompilationUnit unit) {
		System.out.println(unit.getTypeDecl(0).getBodyDecl(0).ASTDotSpec());
  	}
	
	@Override
	protected void initOptions() {
		super.initOptions();
		AST.Options options = program.options();
		options.addKeyOption("--rigid");
		options.addKeyOption("--nodeNbrs");
	}

	@Override
	protected String name() {
		return "JastAddJ-IntraFlow";
	}

	@Override
	protected String version() {
		return "To be added";
	}
}
