package util;

import AST.BytecodeParser;
import AST.CompilationUnit;
import AST.Frontend;
import AST.JavaParser;

/**
 * Prints a dot specification of the CFG with control flow annotations to System.out
 * @author dt09hg0
 *
 */
public class DotGraphPrinter extends Frontend {
	public static void main(String args[]) {
		printCFG(args);
	}

	public static int printCFG(String args[]) {
		int result = new DotGraphPrinter().run(
				args,
				new BytecodeParser(),
				new JavaParser() {
					@Override
					public CompilationUnit parse(java.io.InputStream is,
							String fileName) throws java.io.IOException,
							beaver.Parser.Exception {

						return new parser.JavaParser().parse(is, fileName);
					}
				});
		return result;
	}

	protected void processNoErrors(CompilationUnit unit) {
		System.out.print(unit.getTypeDecl(0).getBodyDecl(0).CFGDotSpec());
//		System.out.println(TestUtil.printCFG(unit.getTypeDecl(0).getBodyDecl(0)));
  	}

	@Override
	protected String name() {
		return "JastAddJ-IntraFlow";
	}

	@Override
	protected String version() {
		return "To be added";
	}
}
