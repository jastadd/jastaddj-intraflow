package util;

import AST.BytecodeParser;
import AST.CompilationUnit;
import AST.Frontend;
import AST.JavaParser;

/**
 * Prints the control flow to System.out
 * 
 * @author dt09hg0
 * 
 */
public class FlowPrinter extends Frontend {

	private final JavaParser parser;
	private final BytecodeParser bytecodeParser;

	public FlowPrinter() {
		parser = new JavaParser() {
			@Override
			public CompilationUnit parse(java.io.InputStream is, String fileName)
					throws java.io.IOException, beaver.Parser.Exception {

				return new parser.JavaParser().parse(is, fileName);
			}
		};
		bytecodeParser = new BytecodeParser();
	}

	public static void main(String args[]) {
		int exitCode = new FlowPrinter().run(args);
		if (exitCode != 0) {
			System.exit(exitCode);
		}
	}

	public int run(String args[]) {
		return run(args, bytecodeParser, parser);
	}

	protected void processNoErrors(CompilationUnit unit) {
		AST.BodyDecl bd = unit.getTypeDecl(0).getBodyDecl(0);
		System.out.println(bd.dumpString());
		System.out.println(util.TestUtil.printCFG(bd));
	}

	@Override
	protected void initOptions() {
		super.initOptions();
		AST.Options options = program.options();
		options.addKeyOption("--rigid");
		options.addKeyOption("--nodeNbrs");
	}

	@Override
	protected String name() {
		return "JastAddJ-IntraFlow";
	}

	@Override
	protected String version() {
		return "To be added";
	}
}
